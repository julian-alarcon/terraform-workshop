provider "aws" {
  region  = "us-east-1"
  version = "1.22.0"
  shared_credentials_file = "~/.aws/credentials"
}

module "stg_frontend" {
  source = "./modules/frontend"
  environment = "stg"
  min_size = 1
  max_size = 2
}
module "prod_frontend" {
  source = "./modules/frontend"
  environment = "prod"
  min_size = 1
  max_size = 2
}
