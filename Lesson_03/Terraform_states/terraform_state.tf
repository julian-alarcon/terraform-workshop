provider "aws" {
  region  = "us-east-1"
  version = "1.22.0"
}
resource "aws_s3_bucket" "terraform_state_s3_bucket" {
  bucket = "ci-cd-terraform-state"
  
  versioning {
    enabled = true
  }
  
  lifecycle {
    prevent_destroy = true
  }
  
  tags {
    Name = "S3 Remote Terraform State Store"
  }
}

resource "aws_dynamodb_table" "terraform_state_lock_dynamodb_table" {
  name           = "terraform-state-lock"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name = "DynamoDB Terraform State Lock Table"
  }
}