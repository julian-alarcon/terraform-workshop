#Store tfstate files in S3
terraform {
  required_version = ">= 0.11.7"
  backend "s3" {
    encrypt        = true
    bucket         = "ci-cd-terraform-state"
    dynamodb_table = "terraform-state-lock"
    region         = "us-east-1"
    key            = "locking_states/terraform.tfstate"
  }
}
provider "aws" {
  region  = "us-east-1"
  version = "1.22.0"
  shared_credentials_file = "~/.aws/credentials"
}

module "stg_frontend" {
  source = "./modules/frontend"
  environment = "stg"
  min_size = 1
  max_size = 2
}
module "prod_frontend" {
  source = "./modules/frontend"
  environment = "prod"
  min_size = 1
  max_size = 2
}
