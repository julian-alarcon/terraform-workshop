provider "aws" {
  region  = "us-east-1"
  version = "1.22.0"
}

resource "aws_launch_configuration" "launch_configuration" {
  name = "${var.environment}-launch-configuration-instance"
  image_id = "ami-04681a1dbd79675a5"
  instance_type = "t2.micro"
}
resource "aws_elb" "elb" {
  name = "${var.environment}-elb"
  security_groups = ["sg-31091c79"]
  availability_zones = ["us-east-1a"]

  listener {
    instance_port = 8000
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }
  
}

resource "aws_autoscaling_group" "autoscaling_group" {
  launch_configuration = "${aws_launch_configuration.launch_configuration.id}"
  availability_zones = ["us-east-1a"]
  min_size = "${var.min_size}"
  max_size = "${var.max_size}"
}
