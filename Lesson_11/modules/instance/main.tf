resource "aws_instance" "instance_ws" {
  ami                         = "${var.ami}"
  availability_zone           = "${var.availability_zone}"
  instance_type               = "${var.instance_type}"

  tags {
    Name = "${var.instance_type}-${var.ami}"
  }
}
