terraform {
  required_version = ">= 0.11.7"
   backend "s3" {
    encrypt        = true
    bucket         = "ci-cd-terraform-state"
    dynamodb_table = "terraform-state-lock"
    region         = "us-east-1"
    key            = "lesson_05/backend/terraform.tfstate"
  } 
}
provider "aws" {
  region  = "us-east-1"
  version = "1.22.0"
  shared_credentials_file = "~/.aws/credentials"
}

module "backend_instance" {
  source                      = "../modules/instance"
  ami                         = "ami-04681a1dbd79675a5"
  availability_zone           = "us-east-1a"
  instance_type               = "t2.micro"
}