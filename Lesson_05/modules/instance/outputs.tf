output "instance_id" {
  value = "${aws_instance.instance_ws.id}"
}

output "instance_az" {
  value = "${aws_instance.instance_ws.availability_zone}"
}

output "instance_key_name" {
  value = "${aws_instance.instance_ws.key_name}"
}

output "instance_public_ip" {
  value = "${aws_instance.instance_ws.public_ip}"
}

output "instance_private_ip" {
  value = "${aws_instance.instance_ws.private_ip}"
}
