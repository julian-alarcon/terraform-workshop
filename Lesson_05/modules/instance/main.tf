resource "aws_instance" "instance_ws" {
  ami                         = "${var.ami}"
  availability_zone           = "${var.availability_zone}"
  instance_type               = "${var.instance_type}"
  #subnet_id                   = "${var.subnet_id}"
}
