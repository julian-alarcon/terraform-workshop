output "prod_instance_id" {
  value = "${module.backend_instance.instance_id}"
}

output "prod_instance_az" {
  value = "${module.backend_instance.instance_az}"
}

output "prod_instance_key_name" {
  value = "${module.backend_instance.instance_key_name}"
}

output "prod_instance_public_ip" {
  value = "${module.backend_instance.instance_public_ip}"
}

output "prod_instance_private_ip" {
  value = "${module.backend_instance.instance_private_ip}"
}
