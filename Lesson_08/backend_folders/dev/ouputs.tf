output "dev_instance_id" {
  value = "${module.backend_instance.instance_id}"
}

output "dev_instance_az" {
  value = "${module.backend_instance.instance_az}"
}

output "dev_instance_key_name" {
  value = "${module.backend_instance.instance_key_name}"
}

output "dev_instance_public_ip" {
  value = "${module.backend_instance.instance_public_ip}"
}

output "dev_instance_private_ip" {
  value = "${module.backend_instance.instance_private_ip}"
}
