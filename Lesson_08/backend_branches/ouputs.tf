output "instance_id" {
  value = "${module.backend_instance.instance_id}"
}

output "instance_az" {
  value = "${module.backend_instance.instance_az}"
}

output "instance_key_name" {
  value = "${module.backend_instance.instance_key_name}"
}

output "instance_public_ip" {
  value = "${module.backend_instance.instance_public_ip}"
}

output "instance_private_ip" {
  value = "${module.backend_instance.instance_private_ip}"
}
