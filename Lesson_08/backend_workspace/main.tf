terraform {
  required_version = ">= 0.11.7"
  backend "s3" {
    encrypt        = true
    bucket         = "ci-cd-terraform-state"
    dynamodb_table = "terraform-state-lock"
    region         = "us-east-1"
    key            = "lesson_08/backend_workspace/terraform.tfstate"
  }
}
provider "aws" {
  region  = "us-east-1"
  version = "1.22.0"
  shared_credentials_file = "~/.aws/credentials"
}

locals {
  env1="${terraform.workspace}"

  amis = {
    "default" = "ami-04681a1dbd79675a5" # Amazon Linux 2
    "prod" = "ami-0ff8a91507f77f867" # Amazon Linux 2018.03.0
  }

  instances = {
    "default" = "t3.micro"
    "prod" = "t2.micro"
  }

  ami = "${lookup(local.amis,local.env1)}"
  instance_type="${lookup(local.instances,local.env1)}"
}

module "backend_instance" {
  source                      = "../modules/instance"
  ami                         = "${local.ami}"
  availability_zone           = "us-east-1a"
  instance_type               = "${local.instance_type}"
}