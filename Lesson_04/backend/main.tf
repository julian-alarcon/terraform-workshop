#Store tfstate files in S3
terraform {
  required_version = ">= 0.11.7"
  backend "s3" {
    encrypt        = true
    bucket         = "ci-cd-terraform-state"
    dynamodb_table = "terraform-state-lock"
    region         = "us-east-1"
    key            = "backend/terraform.tfstate"
  }
}
provider "aws" {
  region  = "us-east-1"
  version = "1.22.0"
  shared_credentials_file = "~/.aws/credentials"
}

module "backend" {
  source = "./../modules/asg"
  app = "back"
  min_size = 1
  max_size = 2
}