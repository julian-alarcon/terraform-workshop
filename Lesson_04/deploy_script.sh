#!/bin/bash

WORK_DIR=$(pwd)

deploy () {
    terraform get -update
    terraform init
    terraform plan
    terraform apply -auto-approve
}
undeploy () {
    terraform destroy -auto-approve
}
###################### Frontend ######################

cd $WORK_DIR/frontend
deploy
#undeploy
cd ..

###################### Backend #####################

cd $WORK_DIR/backend
deploy
#undeploy
