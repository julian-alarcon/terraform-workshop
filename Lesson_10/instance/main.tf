#Store tfstate files in S3
terraform {
  required_version = ">= 0.11.7"
  backend "s3" {
    encrypt        = true
    bucket         = "ci-cd-terraform-state"
    dynamodb_table = "terraform-state-lock"
    region         = "us-east-1"
    key            = "instance/terraform.tfstate"
  }
}
provider "aws" {
  region  = "us-east-1"
  version = "1.22.0"
  shared_credentials_file = "~/.aws/credentials"
}
data "terraform_remote_state" "vpc_state" {
  backend = "s3"
  config {
    bucket = "ci-cd-terraform-state"
    key = "vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

data "aws_ami" "linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.20180810-x86_64-gp2*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["137112412989"]
}

resource "aws_instance" "instance" {
  ami = "${data.aws_ami.linux.id}"
  instance_type = "t2.micro"
  subnet_id = "${data.terraform_remote_state.vpc_state.subnet_id}"
}
