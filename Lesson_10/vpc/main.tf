#Store tfstate files in S3
terraform {
  required_version = ">= 0.11.7"
  backend "s3" {
    encrypt        = true
    bucket         = "ci-cd-terraform-state"
    dynamodb_table = "terraform-state-lock"
    region         = "us-east-1"
    key            = "vpc/terraform.tfstate"
  }
}
provider "aws" {
  region  = "us-east-1"
  version = "1.22.0"
  shared_credentials_file = "~/.aws/credentials"
}
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
}
resource "aws_subnet" "subnet" {
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.1.0/24"
}

