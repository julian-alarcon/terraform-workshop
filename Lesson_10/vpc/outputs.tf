output "subnet_id" {
  value = "${aws_subnet.subnet.id}"
}
output "subnet_cidr_block" {
  value = "${aws_subnet.subnet.cidr_block}"
}
